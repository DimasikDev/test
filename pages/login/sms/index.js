import Head from 'next/head'
import {Component} from "react";
import $ from 'jquery';
import 'imask/dist/imask.min'
import {Main} from "../../main";

export class LoginSms extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>SwapMap - Пин-код</title>
                </Head>
                <Main>
                    <div className="container main">
                        <div className="container-sm login-container">
                            <div className="card login-card">
                                <img className="mb-6" src="/Logo%20large.png" style={{objectFit: 'contain'}}/>
                                <div className="card-body">
                                    <form noValidate action="/registration/create-password" className="needs-validation"
                                          id="login-sms-form">
                                        <div className="form-group mb-3">
                                            <label className="form-label" htmlFor="smsCode">Введите код из
                                                СМС-сообщения</label>
                                            <div className="form-control me-1">
                                                <input placeholder="Код из СМС" minLength="700" name="smsCode"
                                                       id="smsCode" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="send-again mb-3" id="send-again">
                                            Повторная отправка будет доступна через 04:30
                                        </div>
                                        <a href=" " className="send-again-link mb-3" id="send-again-link">
                                            Отправить повторно
                                        </a>
                                        <button id="continue-button" className="btn button large" disabled>Продолжить
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal" id="error-message" data-bs-backdrop="static" data-bs-keyboard="false"
                         tabIndex="-1"
                         aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered modal-sm">
                            <div className="modal-content text-center">
                                <div className="modal-body">
                                    <div className="image text-center mb-4">
                                        <img src="/icons/Error.svg"/>
                                    </div>
                                    Вы использовали 10 неудачных попыток ввода кода из СМС. Следующий раз запросить код
                                    можно будет через
                                    <span style={{color: 'red'}}> 24 часа</span>.
                                    <button type="button" className="btn button large mt-4"
                                            data-bs-dismiss="modal">Оk</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Main>
            </div>
        )
    }
    componentDidMount() {
        $(function () {

            let focusedInput

            $('input').on('focus', function (e) {
                if ($(e.currentTarget).attr('disabled')) return
                let button = document.createElement('div')
                button.classList = 'clear-button'
                focusedInput = $(e.currentTarget)
                button.addEventListener('click', function (e) {
                    e.preventDefault()
                    e.stopPropagation()
                    focusedInput[0].value = ''
                    focusedInput.trigger('input')
                })
                e.currentTarget.parentElement.append(button)
            })
            $('input').on('blur', function (e) {
                e.preventDefault()
                e.stopPropagation()
                setTimeout(() => $(e.currentTarget.parentElement).find('.clear-button').remove(), 50)
            })

            let failCount = 9
            let timerId = null

            startTimer(10)

            function startTimer(time) {
                if (time === 0) {
                    $('#send-again').css('display', 'none')
                    $('#send-again-link').css('display', 'block')
                    return
                } else {
                    $('#send-again-link').css('display', 'none')
                }

                let minutes = (time / 60) | 0
                let sec = time - minutes * 60

                let minStr = '0' + minutes
                let secStr = sec < 10 ? '0' + sec : sec

                $('#send-again')[0].innerHTML = 'Повторная отправка будет доступна через ' + minStr + ':' + secStr
                $('#send-again').css('display', 'block')

                timerId = setTimeout(function () {
                    startTimer(time - 1)
                }, 1000)
            }

            $('#send-again-link').on('click', function () {
                $('#smsCode')[0].value = ''
                startTimer(270)
            })

            let button = $('#continue-button');

            let element = $('#smsCode')[0];
            IMask(element, {
                mask: '000000'
            });

            $('#smsCode').on('input', function (e) {
                e.target.classList.remove('is-invalid')
                e.target.parentElement.classList.remove('is-invalid')
                if (e.target.value && e.target.value.length === 6) {
                    button.removeAttr('disabled')
                } else {
                    button.attr('disabled', 'disabled')
                }
            })

            $('#login-sms-form').on('submit', function (e) {
                clearTimeout(timerId)
                $('#send-again').css('display', 'none')
                $('#send-again-link').css('display', 'none')
                if ($('#smsCode')[0].value !== '123456') {
                    e.preventDefault()
                    failCount++
                    if (failCount >= 10) {
                        $('#error-message').css('display', 'block')
                        $('#error-message')[0].classList.add('show')
                        $('#error-message').find('button')[0].addEventListener('click', () => {
                            $('#error-message')[0].classList.remove('show')
                            $('#error-message').css('display', 'none')
                        })
                    }
                    $('#smsCode')[0].classList.add('is-invalid')
                    $('#smsCode')[0].parentElement.classList.add('is-invalid')
                    button.attr('disabled', 'disabled')

                    if ($('#invalid-message')[0]) return
                    let invalidMessage = document.createElement('div');
                    invalidMessage.classList = 'mb-3 invalid-message'
                    invalidMessage.id = 'invalid-message'
                    let sendAgainLink = document.createElement('a');
                    sendAgainLink.href = ''
                    sendAgainLink.addEventListener('click', function () {
                        $('#smsCode')[0].value = ''
                        startTimer(270)
                        invalidMessage.remove()
                    })
                    invalidMessage.innerHTML = '<span>Неверный код, </span>'
                    sendAgainLink.innerHTML = 'сбросить и прислать новый'
                    invalidMessage.appendChild(sendAgainLink)
                    $($('#smsCode')[0].parentElement.parentElement).after(invalidMessage)

                }

            })
        })
    }
}

export default LoginSms
