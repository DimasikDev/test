import Head from 'next/head'
import {Component} from "react";
import $ from 'jquery';
import {Main} from "../../main";

export class PinCode extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>SwapMap - Пин-код</title>
                </Head>
                <Main>
                    <div className="container main">
                        <div className="container-sm login-container">
                            <div className="card login-card">
                                <img className="mb-6" src="/Logo%20large.png" style={{objectFit: 'contain'}}/>
                                <div className="card-body">
                                    <form noValidate action="/" className="needs-validation"
                                          id="login-pin-code">
                                        <div className="form-group mb-3">
                                            <div className="text-center" style={{fontSize: '25px'}}>Введите код для входа
                                            </div>
                                            <div className="pin-code-wrapper">
                                                <div className="hidden-pin-code" contentEditable="true"
                                                     id="hidden-pin-code">
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                </div>
                                                <input minLength="700" name="pinCode" id="pinCode" type="hidden"
                                                       className="form-control"/>
                                            </div>
                                        </div>
                                        <button id="continue-button" className="btn button large" disabled>Продолжить
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </Main>
            </div>
        )
    }
    componentDidMount() {
        $(function () {
            let pinCode = $('#pinCode');
            let button = $('#continue-button');

            $('#hidden-pin-code').on('keydown', function (e) {
                e.stopPropagation()
                e.preventDefault()


                if (e.originalEvent.key == parseInt(e.originalEvent.key)) {
                    if (pinCode[0].value.length >= 4) return;
                    pinCode[0].value += e.originalEvent.key
                    let star = $('#hidden-pin-code').find('.star')[(pinCode[0].value ? pinCode[0].value.length : 1) - 1];
                    if (star) {
                        star.innerHTML = e.originalEvent.key
                        star.classList.remove('active')
                    }
                    pinCode.trigger('input')
                } else if ('Backspace' === e.originalEvent.key) {
                    pinCode[0].value = pinCode[0].value.slice(0, -1)
                    $('#hidden-pin-code').find('.star')[pinCode[0].value ? pinCode[0].value.length : 0].innerHTML = '*'
                    pinCode.trigger('input')
                } else {
                    return
                }
                setCaret()
            })

            $('#hidden-pin-code').on('click', function (e) {
                setCaret()
            })

            $('#hidden-pin-code').on('blur', function (e) {
                for (let star of $('#hidden-pin-code').find('.star')) {
                    star.classList.remove('active')
                }
            })

            function setCaret() {
                var el = $('#hidden-pin-code')[0]
                el.blur()
                var range = document.createRange()
                var sel = window.getSelection()

                let newVar = pinCode[0].value ? pinCode[0].value.length : 0;
                let star = $('#hidden-pin-code').find('.star')[newVar];
                if (star) {
                    if (!star.classList.contains('active')) star.classList.add('active')
                }
                range.setStart(el, newVar)
                range.collapse(true)

                sel.removeAllRanges()
                sel.addRange(range)
                el.focus()
            }

            pinCode.on('input', function () {
                $('#floating-error').remove()
                if (pinCode[0].value.length === 4) {
                    button.removeAttr('disabled')
                } else {
                    button.attr('disabled', 'disabled')
                }
            })

            $('#auth-pin-code').on('submit', function (e) {
                e.stopPropagation()
                e.preventDefault()

                pinCode.attr('disabled', 'disabled')
                setTimeout(() => button.attr('disabled', 'disabled'))

                $('#floating-error').remove()
                $('#pin-code-error').remove()

                if (pinCode[0].value != '1234') {
                    if ($('#hidden-pin-code').find('#floating-error').length === 0) {
                        let img = document.createElement('img');
                        img.classList = 'floating-error'
                        img.src = '/icons/Error.svg'
                        img.id = 'floating-error'
                        $('#hidden-pin-code')[0].parentElement.append(img)
                        $('#hidden-pin-code').after('<div id="pin-code-error" class="invalid-message mb-3">Неверный код, <a href="/sms">сбросить и прислать новый</a></div>')
                    }
                } else {
                    button.removeAttr('disabled')
                    pinCode.removeAttr('disabled')
                }
            })

        })
    }
}

export default PinCode
