import Head from 'next/head'
import {Component} from "react";
import $ from 'jquery';
import {Main} from "../../../main";

export class PinCodeReset extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>SwapMap - Пин-код - Создать пароль</title>
                </Head>
                <Main>
                    <div className="container main">
                        <div className="container-sm login-container">
                            <div className="card login-card">
                                <img className="mb-6" src="/Logo%20large.png" style={{objectFit: 'contain'}}/>
                                <div className="card-body">
                                    <form noValidate action="/login/pin-code" className="needs-validation"
                                          id="login-pin-code">
                                        <div className="form-group mb-3">
                                            <div className="text-center" style={{fontSize: '25px'}}>Введите код из СМС
                                            </div>
                                            <div className="pin-code-wrapper">
                                                <div className="hidden-pin-code" contentEditable="true"
                                                     id="hidden-pin-code">
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                    <div className="star">
                                                        <div>*</div>
                                                    </div>
                                                </div>
                                                <input minLength="700" name="pinCode" id="pinCode" type="hidden"
                                                       className="form-control" />
                                            </div>
                                            <div className="send-again mb-3" id="send-again">
                                                Повторная отправка будет доступна через 04:30
                                            </div>
                                            <a href=" " className="send-again-link mb-3" id="send-again-link">
                                                Отправить повторно
                                            </a>
                                        </div>
                                        <button id="continue-button" className="btn button large" disabled>Продолжить
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id="error-message" data-bs-backdrop="static" data-bs-keyboard="false"
                         tabIndex="-1"
                         aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered modal-sm">
                            <div className="modal-content text-center">
                                <div className="modal-body">
                                    <div className="image text-center mb-4">
                                        <img src="../assets/icons/Error.svg"/>
                                    </div>
                                    Вы использовали 10 неудачных попыток ввода кода из СМС. Следующий раз запросить код
                                    можно буде через
                                    <span style={{color: 'red'}}>24 часа</span>.
                                    <button type="button" className="btn button large mt-4"
                                            data-bs-dismiss="modal">Оk</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Main>
            </div>
    )
    }
    componentDidMount()
        {
            $(function () {
                let pinCode = $('#pinCode');
                let button = $('#continue-button');

                let failCount = 9
                let timerId = null

                startTimer(10)

                function startTimer(time) {
                    if (time === 0) {
                        $('#send-again').css('display', 'none')
                        $('#send-again-link').css('display', 'block')
                        return
                    } else {
                        $('#send-again-link').css('display', 'none')
                    }

                    let minutes = (time / 60) | 0
                    let sec = time - minutes * 60

                    let minStr = '0' + minutes
                    let secStr = sec < 10 ? '0' + sec : sec

                    $('#send-again')[0].innerHTML = 'Повторная отправка будет доступна через ' + minStr + ':' + secStr
                    $('#send-again').css('display', 'block')

                    timerId = setTimeout(function () {
                        startTimer(time - 1)
                    }, 1000)
                }

                $('#hidden-pin-code').on('keydown', function (e) {
                    e.stopPropagation()
                    e.preventDefault()


                    if (e.originalEvent.key == parseInt(e.originalEvent.key)) {
                        if (pinCode[0].value.length >= 4) return;
                        pinCode[0].value += e.originalEvent.key
                        let star = $('#hidden-pin-code').find('.star')[(pinCode[0].value ? pinCode[0].value.length : 1) - 1];
                        if (star) {
                            star.innerHTML = e.originalEvent.key
                            star.classList.remove('active')
                        }
                        pinCode.trigger('input')
                    } else if ('Backspace' === e.originalEvent.key) {
                        pinCode[0].value = pinCode[0].value.slice(0, -1)
                        $('#hidden-pin-code').find('.star')[pinCode[0].value ? pinCode[0].value.length : 0].innerHTML = '*'
                        pinCode.trigger('input')
                    } else {
                        return
                    }
                    setCaret()
                })

                $('#hidden-pin-code').on('click', function (e) {
                    setCaret()
                })

                $('#hidden-pin-code').on('blur', function (e) {
                    for (let star of $('#hidden-pin-code').find('.star')) {
                        star.classList.remove('active')
                    }
                })

                function setCaret() {
                    var el = $('#hidden-pin-code')[0]
                    el.blur()
                    var range = document.createRange()
                    var sel = window.getSelection()

                    let newVar = pinCode[0].value ? pinCode[0].value.length : 0;
                    let star = $('#hidden-pin-code').find('.star')[newVar];
                    if (star) {
                        if (!star.classList.contains('active')) star.classList.add('active')
                    }
                    range.setStart(el, newVar)
                    range.collapse(true)

                    sel.removeAllRanges()
                    sel.addRange(range)
                    el.focus()
                }

                pinCode.on('input', function () {
                    $('#floating-error').remove()
                    if (pinCode[0].value.length === 4) {
                        button.removeAttr('disabled')
                    } else {
                        button.attr('disabled', 'disabled')
                    }
                })

                $('#auth-pin-code').on('submit', function (e) {
                    e.stopPropagation()
                    e.preventDefault()

                    pinCode.attr('disabled', 'disabled')
                    setTimeout(() => button.attr('disabled', 'disabled'))

                    $('#floating-error').remove()
                    $('#pin-code-error').remove()

                    if (pinCode[0].value != '1234') {
                        clearInterval(timerId)
                        $('#send-again').css('display', 'none')
                        $('#send-again-link').css('display', 'none')

                        failCount++
                        if (failCount >= 10) {
                            $('#error-message').css('display', 'block')
                            $('#error-message')[0].classList.add('show')
                            $('#error-message').find('button')[0].addEventListener('click', () => {
                                $('#error-message')[0].classList.remove('show')
                                $('#error-message').css('display', 'none')
                            })
                        }

                        if ($('#hidden-pin-code').find('#floating-error').length === 0) {
                            let img = document.createElement('img');
                            img.classList = 'floating-error'
                            img.src = '../assets/icons/Error.svg'
                            img.id = 'floating-error'
                            $('#hidden-pin-code')[0].parentElement.append(img)
                            $('#hidden-pin-code').after('<div id="pin-code-error" class="invalid-message mb-3">Неверный код, <a href=" ">сбросить и прислать новый</a></div>')
                        }
                    } else {
                        button.removeAttr('disabled')
                        pinCode.removeAttr('disabled')
                    }
                })

            })
        }
    }

    export default PinCodeReset
