import Head from 'next/head'
import {Component} from "react";
import 'selectize/dist/css/selectize.css';
import $ from 'jquery';
import {Main} from "../main";

export class Login extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>SwapMap</title>
                </Head>
                <Main>
                    <div className="container main">
                        <div className="container-sm login-container">
                            <div className="card login-card">
                                <img className="mb-6" src="/Logo%20large.png" style={{objectFit: 'contain'}}/>
                                <div className="card-body">
                                    <form action="/login/sms" id="login-form">
                                        <div className="form-group mb-3">
                                            <label className="form-label" htmlFor="country">Выберите страну</label>
                                            <select style={{display: 'none'}} id="country" name="country"
                                                    aria-label="Default select example">
                                                <option value="Россия" selected>Россия</option>
                                            </select>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label">Введите номер телефона</label>
                                            <div className="d-flex">
                                                <div className="form-control me-1 disabled">
                                                    <input id="tel-input-code" value="+7" disabled
                                                           style={{width: '84px', textAlign: 'center'}}
                                                           className="form-control"/>
                                                </div>
                                                <div className="form-control">
                                                    <input name="tel" type="tel" id="tel-input" className="form-control"
                                                           placeholder="999 999 9999"/>
                                                </div>
                                            </div>
                                        </div>
                                        <button id="continue-button" className="btn button large" disabled>Продолжить
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </Main>
            </div>
        )
    }
    componentDidMount() {
        require('selectize')
        require('imask')
        $(function () {
            let focusedInput

            $('#tel-input').on('focus', function (e) {
                $('#tel-input-code')[0].classList.add('active')
            })

            $('#tel-input').on('blur', function (e) {
                if (!$('#tel-input').is(':hover')) {
                    $('#tel-input-code')[0].classList.remove('active')
                }
            })

            $('#tel-input').on('mouseenter', function (e) {
                $('#tel-input-code')[0].classList.add('active')
            })

            $('#tel-input').on('mouseleave', function (e) {
                if (!$('#tel-input').is(':focus')) {
                    $('#tel-input-code')[0].classList.remove('active')
                }
            })

            $('input').on('focus', function (e) {
                if ($(e.currentTarget).attr('disabled')) return
                let button = document.createElement('div')
                button.classList = 'clear-button'
                focusedInput = $(e.currentTarget)
                button.addEventListener('click', function (e) {
                    e.preventDefault()
                    e.stopPropagation()
                    focusedInput[0].value = ''
                    focusedInput.trigger('input')
                })
                e.currentTarget.parentElement.append(button)
            })
            $('input').on('blur', function (e) {
                e.preventDefault()
                e.stopPropagation()
                setTimeout(() => $(e.currentTarget.parentElement).find('.clear-button').remove(), 50)
            })

            $('#country').selectize({
                valueField: 'value',
                labelField: 'name',
                options: [
                    {value: 'Россия', name: 'Россия', code: '+7'},
                    {value: 'Россия1', name: 'Россия', code: '+7'},
                    {value: 'Россия2', name: 'Россия', code: '+7'},
                    {value: 'Беларусь', name: 'Беларусь', code: '+7'},
                    {value: 'Беларусь1', name: 'Беларусь', code: '+7'},
                    {value: 'Беларусь2', name: 'Беларусь', code: '+7'},
                    {value: 'Беларусь3', name: 'Беларусь', code: '+7'},
                    {value: 'Украина', name: 'Украина', code: '+7'},
                    {value: 'Украина1', name: 'Украина', code: '+7'},
                    {value: 'Украина2', name: 'Украина', code: '+7'},
                    {value: 'Украина3', name: 'Украина', code: '+7'},
                ],
                create: false,
                render: {
                    option: function (item, escape) {
                        return `<div class="option">` + item.name +
                            '<div>' + item.code + '</div> ' +
                            '</div>';
                    }
                },
                load: function (query, callback) {
                    callback([
                        {value: 'Россия', name: 'Россия', code: '+7'},
                        {value: 'Россия1', name: 'Россия', code: '+7'},
                        {value: 'Россия2', name: 'Россия', code: '+7'},
                        {value: 'Беларусь', name: 'Беларусь', code: '+7'},
                        {value: 'Беларусь1', name: 'Беларусь', code: '+7'},
                        {value: 'Беларусь2', name: 'Беларусь', code: '+7'},
                        {value: 'Беларусь3', name: 'Беларусь', code: '+7'},
                        {value: 'Украина', name: 'Украина', code: '+7'},
                        {value: 'Украина1', name: 'Украина', code: '+7'},
                        {value: 'Украина2', name: 'Украина', code: '+7'},
                        {value: 'Украина3', name: 'Украина', code: '+7'},
                    ])
                }
            });
            let opacityLayer = document.createElement('div');
            opacityLayer.classList = 'opacity-layer'
            opacityLayer.id = 'opacity-layer'
            let dropdown = $('.selectize-dropdown-content');
            dropdown[0].parentElement.style.opacity = 0
            $('.selectize-input').click()
            setTimeout(() => {
                dropdown[0].parentElement.style.display = 'block'
                if (dropdown[0].scrollHeight > dropdown[0].clientHeight) {
                    opacityLayer.classList.add('overflow')
                }
                dropdown[0].parentElement.style.display = 'none'
                dropdown[0].parentElement.style.opacity = 1
            }, 1000)
            dropdown.before(opacityLayer)
            let element = $('#tel-input')[0];
            var mask = IMask(element, {
                mask: '000-000-0000'
            });
            let oldValue = '';

            let button = $('#continue-button');
            mask.on('complete', () => {
                setTimeout(() => {
                    button.removeAttr('disabled')
                }, 10)
            })

            $(element).on('input', function () {
                if (element.value !== oldValue) {
                    button.attr('disabled', 'disabled')
                    oldValue = element.value
                }
            })

            $('#auth-form').on('submit', function (e) {
                $(e.target).find('div.form-control').css('pointer-events', 'none')
                $(e.target).find('div.form-control').children().attr('disabled', 'disabled')
                $(e.target).find('button').attr('disabled', 'disabled')
            })
        })
    }
}

export default Login
