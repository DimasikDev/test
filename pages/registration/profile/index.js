import Head from 'next/head'
import {Component} from "react";
import {Main} from "../../main";
import $ from 'jquery';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'

export class CreatePassword extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>SwapMap - Пин-код - Создать пароль</title>
                    <link rel="stylesheet" href="https://unpkg.com/jcrop/dist/jcrop.css"/>
                    <script src="https://unpkg.com/jcrop"/>
                </Head>
                <Main>
                    <div className="container-fluid">
                        <div className="mt-3 fs-3 mx-6rem">Заполнение профиля</div>
                        <div className="container-sm main profile-card">
                            <div className="text-center mb-8">
                                <div className="circle empty profile">
                                    <img id="avatar" src="/icons/avatar%201.svg"/>
                                </div>
                            </div>
                            <div className="text-center mb-8">
                                <a href=" " id="add-profile-photo">Добавить фото профиля</a>
                            </div>
                            <div className="m-6 mx-6rem mb-7">
                                <div className="mx-auto d-block w-content position-relative btn-group-wrapper">
                                    <div className="btn-group" role="group">
                                        <label value="male" className="btn radio-label"><span>Мужчина</span></label>
                                        <label value="female" className="btn radio-label"><span>Женщина</span></label>
                                        <label value="other" className="btn radio-label"><span>Другое</span></label>

                                        <input type="hidden" value="other" className="btn-check" name="sex" id="sex"
                                               autoComplete="off"/>

                                    </div>
                                    <div id="selection" className="selection"></div>
                                </div>
                            </div>
                            <div className="card small-padd mb-7">
                                <div className="card-note">Необходимо заполнить</div>
                                <div className="card-body">
                                    <form action="./pages/login-sms.html" className="validation" id="main-form">
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="name">Ваше имя*</label>
                                            <div className="form-control me-1 shadowed">
                                                <input name="name" id="name" className="form-control text-center"
                                                       placeholder="Как вас зовут?"/>
                                            </div>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="nickname">Nickname*</label>
                                            <div className="form-control me-1 shadowed">
                                                <input name="nickname" id="nickname"
                                                       className="form-control text-center"
                                                       placeholder="Выберите себе имя"/>
                                            </div>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="email">Email*</label>
                                            <div className="form-control me-1 shadowed">
                                                <input name="email" id="email" className="form-control text-center"
                                                       placeholder="Мы не пришлем спам, обещаем."/>
                                            </div>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="country">Страна*</label>
                                            <div className="form-control me-1 shadowed">
                                                <input name="country" id="country" className="form-control text-center"
                                                       placeholder="Введите страну"/>
                                            </div>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="city">Город*</label>
                                            <div className="form-control me-1 shadowed">
                                                <input name="city" id="city" className="form-control text-center"
                                                       placeholder="Введите город"/>
                                            </div>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="birthday">Дата
                                                рождения*</label>
                                            <div className="form-control me-1 shadowed">
                                                <input name="birthday" id="birthday"
                                                       className="form-control text-center"
                                                       placeholder="Мы пришлем поздравление!"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="card small-padd has-color-black">
                                <div className="card-note">Заполните по желанию</div>
                                <div className="card-body">
                                    <form noValidate action="./pages/login-sms.html" className="needs-validation">
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="about_me">Обо мне</label>
                                            <div className="form-control me-1 shadowed">
                            <textarea minLength="700" name="about_me" id="about_me" className="form-control"
                                      placeholder="Вы уникальны! Расскажите о себе."></textarea>
                                            </div>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="site">Сайт</label>
                                            <div className="form-control me-1 shadowed">
                                                <input minLength="700" name="site" id="site"
                                                       className="form-control text-center"
                                                       placeholder="Укажите ваш сайт"/>
                                            </div>
                                        </div>
                                        <div className="form-group mb-3">
                                            <label className="form-label text-dark" htmlFor="site">Социальные
                                                сети</label>
                                            <div className="card" style={{padding: '0 0.5rem'}}>
                                                <div className="card-body">
                                                    <div className="form-group mb-3 d-flex">
                                                        <label className="form-label text-dark"
                                                               htmlFor="facebook">Facebook</label>
                                                        <div className="form-control me-1 shadowed">
                                                            <input minLength="700" name="facebook" id="facebook"
                                                                   className="form-control small"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-3 d-flex">
                                                        <label className="form-label text-dark"
                                                               htmlFor="youtube">YouTube</label>
                                                        <div className="form-control me-1 shadowed">
                                                            <input minLength="700" name="youtube" id="youtube"
                                                                   className="form-control small"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-3 d-flex">
                                                        <label className="form-label text-dark"
                                                               htmlFor="instagram">Instagram</label>
                                                        <div className="form-control me-1 shadowed">
                                                            <input minLength="700" name="instagram" id="instagram"
                                                                   className="form-control small"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-3 d-flex">
                                                        <label className="form-label text-dark"
                                                               htmlFor="twitter">Twitter</label>
                                                        <div className="form-control me-1 shadowed">
                                                            <input minLength="700" name="twitter" id="twitter"
                                                                   className="form-control small"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-3 d-flex">
                                                        <label className="form-label text-dark" htmlFor="tik_tok">Tik
                                                            Tok</label>
                                                        <div className="form-control me-1 shadowed">
                                                            <input minLength="700" name="tik_tok" id="tik_tok"
                                                                   className="form-control small"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-3 d-flex">
                                                        <label className="form-label text-dark" htmlFor="vk">Vk</label>
                                                        <div className="form-control me-1 shadowed">
                                                            <input minLength="700" name="vk" id="vk"
                                                                   className="form-control small"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            <div className="text-center mb-6 mt-6">
                                <form action="#" id="reg-text-form">
                                    <button id="continue-button" className="btn button large" form="main-form"
                                            style={{maxWidth: '334px'}}
                                            type="submit">
                                        Продолжить
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id="crop-image-modal" data-bs-backdrop="static" data-bs-keyboard="false"
                         tabIndex="-1"
                         aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered modal-lg">
                            <div className="modal-content text-center">
                                <div className="modal-body">
                                    <div className="mb-7 mt-6 text-center text-nowrap header">
                                        Выберите область фотографии для отображения.
                                    </div>
                                    <div id="small-avatar" className="mb-7"></div>
                                    <div className="text-center mb-6">
                                        <button className="button btn large" style={{width: 'max-content'}}>Подтвердить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Main>
            </div>
        )
    }

    componentDidMount() {
        require('bootstrap-datepicker')
        require('jquery-validation')
        $(function () {
            let focusedInput
            $('#main-form #birthday').datepicker({
                orientation: "bottom left",
                endDate: new Date(),
                startView: 3,
                language: "ru"
            });

            $('input[id=birthday]').on('input', function (e) {
                console.log(e)
            })

            let prev = $('th.prev');
            prev.remove()
            $('th.next').before(prev)

            $('input').on('input', function (e) {
                e.currentTarget.classList.remove('is-invalid')
                e.currentTarget.parentElement.classList.remove('is-invalid')
            })

            $('input').on('focus mouseenter', function (e) {
                if ($(e.currentTarget).attr('disabled') || $(e.currentTarget.parentElement).find('.clear-button').length > 0) return
                let button = document.createElement('div')
                button.classList = 'clear-button'
                focusedInput = $(e.currentTarget)
                button.addEventListener('click', function (e) {
                    e.preventDefault()
                    e.stopPropagation()
                    focusedInput[0].value = ''
                    focusedInput.trigger('input')
                })
                e.currentTarget.parentElement.append(button)
            })
            $('input').on('blur mouseleave', function (e) {
                if (e.type = 'mouseleave' && (e.relatedTarget && e.relatedTarget.classList.contains('clear-button'))) {
                    $(e.target).hover()
                    return
                }
                if ($(e.target).is(':focus')) {
                    return;
                }
                setTimeout(() => $(e.currentTarget.parentElement).find('.clear-button').remove(), 50)
            })

            let selectionWidth = $('.btn-group-wrapper').find('label.btn')[0].clientWidth;
            placeSelection()

            $('.radio-label').on('click', function (e) {
                $('#sex')[0].value = $(e.currentTarget).attr('value')
                setTimeout(() => placeSelection(), 100)
            })

            function placeSelection() {
                $('#selection')[0].style.width = `calc(${selectionWidth}px - 6px)`
                let label = $('.btn-group-wrapper').find(`label[value="${$('#sex')[0].value}"]`)
                $('#selection')[0].style.left = `${label.offset().left - label.parent().offset().left}px`
            }

            $('#add-profile-photo').on('click', function (e) {
                e.preventDefault()
                e.stopPropagation()
                let input = $.parseHTML('<input type="file" accept="image/*" class="invisible-input" name="image">');
                input[0].addEventListener('input', function (e) {
                    input[0].remove();
                    let file = e.target.files[0];
                    let src = URL.createObjectURL(file);
                    $(window).one('click', function () {
                        $('#crop-image-modal')[0].classList.remove('show')
                        $('#crop-image-modal').css('display', 'none')
                    });
                    let exists = $('#small-avatar').find('img').length > 0;
                    let image = exists ? $('#small-avatar').find('img') : new Image();
                    image.src = src
                    if (!exists) $('#small-avatar').append(image)
                    Jcrop.load(image).then(img => {
                        let jcrop = Jcrop.attach(img)
                        jcrop.listen('crop.change', (widget, e) => {
                            console.log(widget.pos);
                        })
                        let rect = Jcrop.Rect.from(img)
                        jcrop.newWidget(rect, {aspectRatio: 1})
                    })
                    $('#crop-image-modal').css('display', 'block')
                    $('#crop-image-modal')[0].classList.add('show')
                    $('#crop-image-modal').find('.modal-dialog').click(function (e) {
                        e.stopPropagation()
                        e.preventDefault()
                    })
                    $('#crop-image-modal').find('button')[0].addEventListener('click', (e) => {
                        e.preventDefault()
                        $('#avatar')[0].parentElement.classList.remove('empty')
                        $('#avatar').attr('src', src)
                        $('#crop-image-modal')[0].classList.remove('show')
                        $('#crop-image-modal').css('display', 'none')
                    })

                    $('#add-profile-photo')[0].innerHTML = 'Изменить фото профиля'
                })
                $('body').append(input)
                $(input[0]).click();
            })

            $('form[id="main-form"]').validate({
                errorClass: 'is-invalid',
                messages: {
                    name: {
                        required: 'Заполните, пожалуйста, Ваше имя',
                        maxlength: 'Достигнута максимальная длина Вашего имени'
                    },
                    nickname: {
                        required: 'Заполните, пожалуйста, Nickname',
                        maxlength: 'Достигнута максимальная длина Nickname'
                    },
                    email: {
                        required: 'Заполните, пожалуйста, email адрес',
                        email: 'Email введён некорректно, проверьте пожалуйста.',
                        maxlength: 'Достигнута максимальная длина email адреса'
                    },
                    country: {
                        required: 'Заполните, пожалуйста, страну',
                        maxlength: 'Достигнута максимальная длина названия страны'
                    },
                    city: {
                        required: 'Заполните, пожалуйста, город',
                        maxlength: 'Достигнута максимальная длина названия города'
                    },
                    birthday: {
                        date: 'Пожалуйста, введите корректную дату',
                        required: 'Заполните, пожалуйста, дату рождения',
                        maxlength: 'Достигнута максимальная длина названия города'
                    },
                },
                rules: {
                    name: {
                        required: true,
                        maxlength: 255
                    },
                    email: {
                        email: true,
                        required: true,
                        maxlength: 255
                    },
                    nickname: {
                        required: true,
                        maxlength: 255
                    },
                    country: {
                        required: true,
                        maxlength: 255
                    },
                    city: {
                        required: true,
                        maxlength: 255
                    },
                    birthday: {
                        required: true,
                        date: true,
                        normalizer: function (value) {
                            let pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
                            value = value.replace(pattern, '$3-$2-$1');
                            let date = Date.parse(value);
                            if (isNaN(date)) return value
                            return new Date(value).toISOString()
                        }
                    },

                }
            });
        })
    }
}

export default CreatePassword
