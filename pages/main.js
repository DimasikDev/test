import {Component} from "react";

export class Main extends Component {
    render() {
        return (
            <main>
                <nav className="navbar navbar-expand-lg navbar-light">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#"><img src="/Logo%20small.png"/></a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <a className="nav-link active" aria-current="page" href="#">Зал славы</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Все награды</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">События</a>
                                </li>

                            </ul>
                            <div className="d-flex" style={{marginLeft: 'auto'}}>
                                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                    <li className="nav-item">
                                        <a className="nav-link" aria-current="page" href="#">
                                            <img src="/icons/location.svg" width="30px" height="30px"/>
                                            Санкт -Петербург
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" aria-current="page" href="#">
                                            <img src="/icons/calendar.svg" width="28px" height="28px"/>
                                            Введите дату
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" aria-current="page" href="#">
                                            Вход / Регистрация
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                {this.props.children}
            </main>
        )
    }
}

export default Main